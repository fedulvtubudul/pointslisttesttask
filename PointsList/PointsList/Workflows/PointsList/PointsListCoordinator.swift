import UIKit
import RxCocoa


class PointsListCoordinator: BaseCoordinator {
	typealias Router = UINavigationController
	
	let context: AppContext
	let router: Router
	
	init(router: Router, context: AppContext) {
		self.router = router
		self.context = context
	}
	
	override func start() {
		CommonTheme.translucentNavigationBar.apply(to: self.router.navigationBar)

		let news = pointsListConfiguration()
		self.router.pushViewController(news, animated: false)
	}
	
	lazy var pointsRequested = Binder<ConfigurationVM>(self) { (shelf, viewModel) in
		guard let count = viewModel.count.value else {
			fatalError("Can not parse points count")
		}
	
		let vc = shelf.pointsList(count: count)
		shelf.router.pushViewController(vc, animated: true)
	}
	
	lazy var errorOccured = Binder<PointsListError>(self) { (shelf, error) in
		let alert = UIAlertController(title: L10n.Error.Alert.title,
			message: error.description,
			preferredStyle: .alert)
		
		let dismissAction = UIAlertAction(title: L10n.Error.Alert.dismiss,
			style: .cancel, handler: { [weak self] (_) in
				self?.router.popViewController(animated: true)
			})
		
		alert.addAction(dismissAction)
		
		self.router.present(alert, animated: true, completion: nil)
	}
	
}

private extension PointsListCoordinator {

	func pointsListConfiguration() -> ConfigurationVC {
		let vc = StoryboardScene.Points.configurationVC.instantiate()
		let vm = ConfigurationVMImpl()
		vc.viewModel = vm
		vc.navigationItem.title = L10n.PointsList.Configuration.NavItem.title
		vc.navigationItem.largeTitleDisplayMode = .always
		
		vc.pointsRequested
			.bind(to: self.pointsRequested)
			.disposed(by: vc.disposeBag)

		return vc
	}
	
	func pointsList(count: Int) -> PointsListVC {
		let vc = StoryboardScene.Points.pointsListVC.instantiate()
		let vm = PointsListVMImpl(context: self.context, count: count)
		vc.viewModel = vm
		vc.navigationItem.title = L10n.PointsList.PointsList.NavItem.title
		vc.navigationItem.largeTitleDisplayMode = .never
		vc.hidesBottomBarWhenPushed = true
		
		vm.errorOccured
			.bind(to: self.errorOccured)
			.disposed(by: vc.disposeBag)
		
		vm.fetch()
		
		return vc
	}
}
