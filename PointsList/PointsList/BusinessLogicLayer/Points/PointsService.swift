import Foundation
import RxSwift


protocol PointsService: class {
	func fetchPoints(count: Int) -> Single<[Point]>
}
