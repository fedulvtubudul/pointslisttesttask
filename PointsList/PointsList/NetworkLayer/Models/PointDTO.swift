import Foundation
import ObjectMapper


class PointDTO: ImmutableMappable {

	let x: Decimal
	let y: Decimal

	required init(map: Map) throws {
		x = try map.value("x", using: DecimalTransform())
		y = try map.value("y", using: DecimalTransform())
	}
	
}
