import Foundation


public enum NetworkLayerError: Error, CustomStringConvertible {
	
	public enum ConnectionFailReasons {
		case noConnection
		case unknown
	}
	
	case unknown
	case badResponse(message: String?)
	case connectionFail(reason: ConnectionFailReasons)
	case businessProblem(code: Int, errorMessage: String?)
	
	public var description: String {
		switch self {
			case let .businessProblem(_, errorMessage):
				return errorMessage ?? L10n.Error.Unknown.message
			case .connectionFail:
				return L10n.Network.Error.ConnectionLost.message
			default:
				return L10n.Error.Unknown.message
		}
	}
}



