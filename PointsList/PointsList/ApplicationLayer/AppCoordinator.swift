import Foundation
import UIKit


class AppCoordinator: BaseCoordinator {
	
	let window: UIWindow
	var context: AppContext
	
	private var mainCoordinator: MainCoordinator?
	
	// MARK: - Lifecycle
	
	init(window: UIWindow, context: AppContext) {
		self.window = window
		self.context = context
	}
	
	// MARK: - Public methods
	
	override func start() {
		CommonTheme.tintedWindow.apply(to: self.window)
		self.chooseFlow()
	}
	
	// MARK: - Private methods
	
	func chooseFlow() {
		self.startMain()
	}
	
	func startMain() {
		let tbc = UITabBarController()
		self.window.changeRootViewController(tbc)
		
		let coordinator = MainCoordinator(router: tbc, context: context)
		self.mainCoordinator = coordinator
		self.addDependency(coordinator)
		coordinator.completionHandler = { [weak self] in
			self?.removeDependency(self?.mainCoordinator)
			self?.mainCoordinator = nil
		}
		coordinator.start()
	}
	
}
