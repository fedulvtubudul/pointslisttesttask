import UIKit
import RxCocoa


class AboutCoordinator: BaseCoordinator {
	typealias Router = UINavigationController
	
	let context: AppContext
	let router: Router
	
	init(router: Router, context: AppContext) {
		self.router = router
		self.context = context
	}
	
	override func start() {
		CommonTheme.translucentNavigationBar.apply(to: self.router.navigationBar)

		let about = aboutScreen()
		self.router.pushViewController(about, animated: false)
	}
	
}

private extension AboutCoordinator {

	func aboutScreen() -> AboutVC {
		let vc = StoryboardScene.About.aboutVC.instantiate()

		vc.navigationItem.title = L10n.About.NavItem.title
		vc.navigationItem.largeTitleDisplayMode = .always

		return vc
	}
	
}
