import UIKit


class PointCell: UITableViewCell {
	
	@IBOutlet var pointXLabel: UILabel!
	@IBOutlet var pointYLabel: UILabel!
	
	override func awakeFromNib() {
		super.awakeFromNib()
		ListTheme.listCell.apply(to: self)
		ListTheme.listCellText.apply(to: self.pointXLabel)
		ListTheme.listCellText.apply(to: self.pointYLabel)
	}
	
}
