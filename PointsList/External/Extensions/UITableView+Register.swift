import UIKit


extension UITableView {
	
	func registerCellFromNibByName(_ name: String) {
		let nib = UINib(nibName: name, bundle: nil)
		self.register(nib, forCellReuseIdentifier: name)
	}
	
}
