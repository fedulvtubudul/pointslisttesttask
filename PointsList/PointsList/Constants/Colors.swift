// swiftlint:disable all
// Generated using SwiftGen, by O.Halligon — https://github.com/SwiftGen/SwiftGen

#if os(OSX)
  import AppKit.NSColor
  internal typealias Color = NSColor
#elseif os(iOS) || os(tvOS) || os(watchOS)
  import UIKit.UIColor
  internal typealias Color = UIColor
#endif

// swiftlint:disable superfluous_disable_command
// swiftlint:disable file_length

// MARK: - Colors

// swiftlint:disable identifier_name line_length type_body_length
internal struct ColorName {
  internal let rgbaValue: UInt32
  internal var color: Color { return Color(named: self) }

  /// <span style="display:block;width:3em;height:2em;border:1px solid black;background:#ffffff"></span>
  /// Alpha: 100% <br/> (0xffffffff)
  internal static let ctaButtonBG = ColorName(rgbaValue: 0xffffffff)
  /// <span style="display:block;width:3em;height:2em;border:1px solid black;background:#efeff4"></span>
  /// Alpha: 100% <br/> (0xefeff4ff)
  internal static let ctaButtonTextDisabled = ColorName(rgbaValue: 0xefeff4ff)
  /// <span style="display:block;width:3em;height:2em;border:1px solid black;background:#584e9f"></span>
  /// Alpha: 53% <br/> (0x584e9f88)
  internal static let ctaButtonTextHighlighted = ColorName(rgbaValue: 0x584e9f88)
  /// <span style="display:block;width:3em;height:2em;border:1px solid black;background:#584e9f"></span>
  /// Alpha: 100% <br/> (0x584e9fff)
  internal static let ctaButtonTextNormal = ColorName(rgbaValue: 0x584e9fff)
  /// <span style="display:block;width:3em;height:2em;border:1px solid black;background:#584e9f"></span>
  /// Alpha: 100% <br/> (0x584e9fff)
  internal static let defaultTint = ColorName(rgbaValue: 0x584e9fff)
  /// <span style="display:block;width:3em;height:2em;border:1px solid black;background:#2a2a2a"></span>
  /// Alpha: 100% <br/> (0x2a2a2aff)
  internal static let fieldTitleText = ColorName(rgbaValue: 0x2a2a2aff)
  /// <span style="display:block;width:3em;height:2em;border:1px solid black;background:#ffffff"></span>
  /// Alpha: 100% <br/> (0xffffffff)
  internal static let inputFieldBG = ColorName(rgbaValue: 0xffffffff)
  /// <span style="display:block;width:3em;height:2em;border:1px solid black;background:#2a2a2a"></span>
  /// Alpha: 100% <br/> (0x2a2a2aff)
  internal static let inputFieldText = ColorName(rgbaValue: 0x2a2a2aff)
  /// <span style="display:block;width:3em;height:2em;border:1px solid black;background:#ffffff"></span>
  /// Alpha: 100% <br/> (0xffffffff)
  internal static let listCellBG = ColorName(rgbaValue: 0xffffffff)
  /// <span style="display:block;width:3em;height:2em;border:1px solid black;background:#2a2a2a"></span>
  /// Alpha: 100% <br/> (0x2a2a2aff)
  internal static let listCellText = ColorName(rgbaValue: 0x2a2a2aff)
  /// <span style="display:block;width:3em;height:2em;border:1px solid black;background:#efeff4"></span>
  /// Alpha: 100% <br/> (0xefeff4ff)
  internal static let panelBG = ColorName(rgbaValue: 0xefeff4ff)
  /// <span style="display:block;width:3em;height:2em;border:1px solid black;background:#2a2a2a"></span>
  /// Alpha: 100% <br/> (0x2a2a2aff)
  internal static let panelTitleText = ColorName(rgbaValue: 0x2a2a2aff)
  /// <span style="display:block;width:3em;height:2em;border:1px solid black;background:#efeff4"></span>
  /// Alpha: 100% <br/> (0xefeff4ff)
  internal static let screenBackground = ColorName(rgbaValue: 0xefeff4ff)
  /// <span style="display:block;width:3em;height:2em;border:1px solid black;background:#2a2a2a"></span>
  /// Alpha: 100% <br/> (0x2a2a2aff)
  internal static let staticText = ColorName(rgbaValue: 0x2a2a2aff)
  /// <span style="display:block;width:3em;height:2em;border:1px solid black;background:#ffffff"></span>
  /// Alpha: 100% <br/> (0xffffffff)
  internal static let staticTextBG = ColorName(rgbaValue: 0xffffffff)
}
// swiftlint:enable identifier_name line_length type_body_length

// MARK: - Implementation Details

// swiftlint:disable operator_usage_whitespace
internal extension Color {
  convenience init(rgbaValue: UInt32) {
    let red   = CGFloat((rgbaValue >> 24) & 0xff) / 255.0
    let green = CGFloat((rgbaValue >> 16) & 0xff) / 255.0
    let blue  = CGFloat((rgbaValue >>  8) & 0xff) / 255.0
    let alpha = CGFloat((rgbaValue      ) & 0xff) / 255.0

    self.init(red: red, green: green, blue: blue, alpha: alpha)
  }
}
// swiftlint:enable operator_usage_whitespace

internal extension Color {
  convenience init(named color: ColorName) {
    self.init(rgbaValue: color.rgbaValue)
  }
}
