import Foundation


protocol PointsServiceContext {
	var pointsService: PointsService { get set }
}


typealias AppContext =
	PointsServiceContext


open class AppContextImpl: AppContext {
	
	var apiProvider: LEOProvider
	var pointsService: PointsService
	let loggingService: LoggingService = LoggingServiceImpl()
	
	
	init() {
		self.apiProvider = LEOProvider()
//		self.apiProvider.authHandler = AuthorizationHandler(accountService: self.accountService)
		self.pointsService = PointsServiceImpl(apiProvider: self.apiProvider)
//		self.pointsService = PointsServiceMock()
	}
	
}
