import UIKit
import MarkdownView


class AboutVC: UIViewController {
	@IBOutlet private var mdView: MarkdownView!
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		self.loadContext()
	}
	
	private func loadContext() {
		if let path = Bundle.main.path(forResource: "About", ofType: "md") {
			let md = try? String(contentsOfFile: path)
			self.mdView.load(markdown: md)
		}
	}
}
