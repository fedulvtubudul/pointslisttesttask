#!/bin/sh

$PODS_ROOT/SwiftGen/bin/swiftgen ib "$PROJECT_DIR" --output "$PROJECT_DIR/$PROJECT_NAME/Constants/Storyboards.swift" -t scenes-swift4

$PODS_ROOT/SwiftGen/bin/swiftgen xcassets "$PROJECT_DIR/$PROJECT_NAME/Resources/Assets.xcassets" --output "$PROJECT_DIR/$PROJECT_NAME/Constants/Images.swift" -t swift4

$PODS_ROOT/SwiftGen/bin/swiftgen fonts "$PROJECT_DIR" --output "$PROJECT_DIR/$PROJECT_NAME/Constants/Fonts.swift" -t swift4

$PODS_ROOT/SwiftGen/bin/swiftgen colors "$PROJECT_DIR/$PROJECT_NAME/Resources/Colors.txt" --output "$PROJECT_DIR/$PROJECT_NAME/Constants/Colors.swift" -t swift4

$PODS_ROOT/SwiftGen/bin/swiftgen strings "$PROJECT_DIR/$PROJECT_NAME/Resources/Localizable.strings" --output "$PROJECT_DIR/$PROJECT_NAME/Constants/Strings.swift" -t structured-swift4
