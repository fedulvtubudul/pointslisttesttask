import Foundation
import RxSwift


class PointsServiceImpl: ReactiveRequestService, PointsService {
	let apiProvider: LEOProvider

	init(apiProvider: LEOProvider) {
		self.apiProvider = apiProvider
	}

	func fetchPoints(count: Int) -> Single<[Point]> {
		let router = PointsRouter.getPoints(count: count)
		return reactiveRequest(type: PointsResponse.self, router: router)
			.map({ (response) -> [Point] in
				return response.points.map({ (dto) -> Point in
					return Point(x: (dto.x as NSNumber).floatValue, y: (dto.y as NSNumber).floatValue)
				})
			})
	}

}
