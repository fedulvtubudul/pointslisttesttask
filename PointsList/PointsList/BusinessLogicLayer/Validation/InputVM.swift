import Foundation
import RxSwift
import RxCocoa


protocol InputVM {
	associatedtype ValueType
	
	var isDisabled: BehaviorRelay<Bool> { get }
	var inputText: BehaviorRelay<String?> { get }
	var errorMessage: BehaviorRelay<String?> { get }
	
	var initialValue: ValueType { get }
	var placeholder: String { get }
	
	var didAdditionalButtonTap: (()->Void)? { get set }
	var validator: StringValidator? { get set }
	
	func validate() -> Bool
	var valueChanged: Bool { get }
}


class TextInputVM: InputVM {
	typealias ValueType = String
	
	var isDisabled: BehaviorRelay<Bool> = BehaviorRelay(value: false)
	
	var inputText: BehaviorRelay<String?> = BehaviorRelay(value: nil)
	var errorMessage: BehaviorRelay<String?> = BehaviorRelay(value: nil)
	
	var didAdditionalButtonTap: (() -> Void)?
	
	var initialValue: String
	var placeholder: String
	
	var validator: StringValidator?
	
	init(placeholder: String, initialValue: String = "") {
		self.placeholder = placeholder
		self.initialValue = initialValue
		self.inputText.accept(initialValue)
	}
	
	func validate() -> Bool {
		guard let v = self.validator else {
			return true
		}
		
		return self.isValidationResultSuccess(v.isValid(value: inputText.value))
	}
	
	func validate(referenceValue: String?) -> Bool {
		guard let v = self.validator else {
			return true
		}
		return self.isValidationResultSuccess(v.isValid(value: inputText.value))
	}
	
	var valueChanged: Bool {
		get {
			let currentValue = self.inputText.value ?? ""
			return currentValue != initialValue
		}
	}
	
	private func isValidationResultSuccess(_ validationResult: ValidationResult) -> Bool {
		switch validationResult {
		case .success:
			self.errorMessage.accept(nil)
			return true
		case let .error(description):
			self.errorMessage.accept(description)
			return false
		}
	}
}
