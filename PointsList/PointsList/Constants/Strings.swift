// swiftlint:disable all
// Generated using SwiftGen, by O.Halligon — https://github.com/SwiftGen/SwiftGen

import Foundation

// swiftlint:disable superfluous_disable_command
// swiftlint:disable file_length

// MARK: - Strings

// swiftlint:disable explicit_type_interface function_parameter_count identifier_name line_length
// swiftlint:disable nesting type_body_length type_name
internal enum L10n {

  internal enum About {
    internal enum NavItem {
      /// About
      internal static let title = L10n.tr("Localizable", "about.navItem.title")
    }
    internal enum TabItem {
      /// About
      internal static let title = L10n.tr("Localizable", "about.tabItem.title")
    }
  }

  internal enum Error {
    internal enum Alert {
      /// OK
      internal static let dismiss = L10n.tr("Localizable", "error.alert.dismiss")
      /// Error
      internal static let title = L10n.tr("Localizable", "error.alert.title")
    }
    internal enum Unknown {
      /// Unknown error occured
      internal static let message = L10n.tr("Localizable", "error.unknown.message")
    }
  }

  internal enum Network {
    internal enum Error {
      internal enum ConnectionLost {
        /// The internet connection was lost, please try again later
        internal static let message = L10n.tr("Localizable", "network.error.connectionLost.message")
      }
    }
  }

  internal enum PointsList {
    internal enum ChartPanel {
      /// Chart
      internal static let title = L10n.tr("Localizable", "pointsList.chartPanel.title")
    }
    internal enum Configuration {
      internal enum Count {
        /// Input points count
        internal static let placeholder = L10n.tr("Localizable", "pointsList.configuration.count.placeholder")
        /// Points count
        internal static let title = L10n.tr("Localizable", "pointsList.configuration.count.title")
      }
      internal enum Info {
        /// This app fetches and displays points array. Please input number of points to proceed.
        internal static let text = L10n.tr("Localizable", "pointsList.configuration.info.text")
        /// Info
        internal static let title = L10n.tr("Localizable", "pointsList.configuration.info.title")
      }
      internal enum NavItem {
        /// Points
        internal static let title = L10n.tr("Localizable", "pointsList.configuration.navItem.title")
      }
      internal enum Proceed {
        /// Proceed
        internal static let title = L10n.tr("Localizable", "pointsList.configuration.proceed.title")
      }
    }
    internal enum PointsList {
      internal enum NavItem {
        /// Points
        internal static let title = L10n.tr("Localizable", "pointsList.pointsList.navItem.title")
      }
      internal enum PointItem {
        /// NA
        internal static let noValue = L10n.tr("Localizable", "pointsList.pointsList.pointItem.noValue")
        /// x: %@
        internal static func xFormat(_ p1: String) -> String {
          return L10n.tr("Localizable", "pointsList.pointsList.pointItem.xFormat", p1)
        }
        /// y: %@
        internal static func yFormat(_ p1: String) -> String {
          return L10n.tr("Localizable", "pointsList.pointsList.pointItem.yFormat", p1)
        }
      }
    }
    internal enum TabItem {
      /// Points
      internal static let title = L10n.tr("Localizable", "pointsList.tabItem.title")
    }
  }
}
// swiftlint:enable explicit_type_interface function_parameter_count identifier_name line_length
// swiftlint:enable nesting type_body_length type_name

// MARK: - Implementation Details

extension L10n {
  private static func tr(_ table: String, _ key: String, _ args: CVarArg...) -> String {
    let format = NSLocalizedString(key, tableName: table, bundle: Bundle(for: BundleToken.self), comment: "")
    return String(format: format, locale: Locale.current, arguments: args)
  }
}

private final class BundleToken {}
