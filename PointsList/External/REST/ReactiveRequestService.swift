import ObjectMapper
import RxSwift

protocol ReactiveRequestService: class {
	var apiProvider: LEOProvider { get }
}

extension ReactiveRequestService {
	func reactiveRequest<T: LEOBaseResponse>(type: T.Type, router: LEORouter) -> Single<T> {
		return Single<T>.create { single in
			let request = self.apiProvider.request(router: router) { (response: Response<T>) in
				switch response {
					
				case let .success(result):
					single(.success(result))
					
				case let .error(error):
					single(.error(error))
				}
			}
			
			return Disposables.create {
				request.cancel()
			}
		}
	}
}
