import UIKit


protocol LEORouter: RequestRouter {}


extension LEORouter {
	func baseUrl() -> URL {
		return URL(string: "https://demo.bankplus.ru/mobws/json")!
	}
}
