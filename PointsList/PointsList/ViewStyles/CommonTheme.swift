import UIKit


struct CommonTheme {
	
	static let translucentNavigationBar = ViewStyle<UINavigationBar> { view in
		view.isTranslucent = true
		view.prefersLargeTitles = true
	}

	static let tintedWindow = ViewStyle<UIWindow> { view in
		view.tintColor = ColorName.defaultTint.color
	}

	static let transparent = ViewStyle<UIView> { view in
		view.backgroundColor = Color.clear
	}

	static let screenBackground = ViewStyle<UIView> { view in
		view.backgroundColor = ColorName.screenBackground.color
	}
}
