import Foundation


protocol PointVM {
	var xTitle: String { get }
	var yTitle: String { get }
}


class PointVMImpl: PointVM {
	
	private let point: Point
	private static let formatter: NumberFormatter = {
		let nf = NumberFormatter()
		nf.numberStyle = .decimal
		nf.maximumFractionDigits = 2
		return nf
	}()

	// MARK: - Lifecycle
	
	init(point: Point) {
		self.point = point
	}

	// MARK: - Public methods

	lazy var xTitle: String = {
		let value = type(of: self).formatter.string(from: self.point.x as NSNumber)
			?? L10n.PointsList.PointsList.PointItem.noValue
		
		return L10n.PointsList.PointsList.PointItem.xFormat(value)
	}()

	lazy var yTitle: String = {
		let value = type(of: self).formatter.string(from: self.point.y as NSNumber)
			?? L10n.PointsList.PointsList.PointItem.noValue
		
		return L10n.PointsList.PointsList.PointItem.yFormat(value)

	}()

}
