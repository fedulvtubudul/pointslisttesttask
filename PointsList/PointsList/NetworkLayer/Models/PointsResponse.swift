import Foundation
import ObjectMapper


class PointsResponse: LEOBaseResponse {
	let points: [PointDTO]
	
	required public init(map: Map) throws {
		points = try map.value("response.points")
		try super.init(map: map)
	}
	
}
