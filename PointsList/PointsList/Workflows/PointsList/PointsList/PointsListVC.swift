import UIKit
import RxSwift
import RxCocoa
import Panels


class PointsListVC: UIViewController {
	var viewModel: PointsListVM!
	
	@IBOutlet private var tableView: UITableView!
	private lazy var panelManager = Panels(target: self)
	
	let disposeBag = DisposeBag()

	
	// MARK: - Lifecycle

	override func viewDidLoad() {
		super.viewDidLoad()
		self.setupStyle()
		self.setupTableView()
		self.bindToVM()
		self.setupChartsPanel()
	}
}


// MARK: - Private methods
private extension PointsListVC {

	func bindToVM() {
		self.viewModel.listItems.subscribe(onNext: { [weak self] (points) in
				self?.tableView.reloadData()
			}).disposed(by: self.disposeBag)
	}
	
	func setupStyle() {
		CommonTheme.screenBackground.apply(to: self.view)
	
		let transparentViews: [UIView] = [
			self.tableView,
		]
		
		transparentViews.forEach(CommonTheme.transparent.apply)
		
	}
	
	func setupTableView() {
		self.tableView.registerCellFromNibByName(PointCell.nameOfClass)
	}
	
	func setupChartsPanel() {
		let panel = StoryboardScene.Points.chartPanelVC.instantiate()
		panel.viewModel = self.viewModel
		panelManager.delegate = panel
		panelManager.show(panel: panel)
		self.additionalSafeAreaInsets = UIEdgeInsets(top: 0, left: 0, bottom: 80, right: 0)
	}
	
}

extension PointsListVC: UITableViewDelegate, UITableViewDataSource {
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return self.viewModel.listItems.value.count
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		guard let cell = tableView.dequeueReusableCell(withIdentifier: PointCell.nameOfClass, for: indexPath) as? PointCell else {
			fatalError("Can not dequeue PointCell cell")
		}
		
		let item = self.viewModel.listItems.value[indexPath.row]
		
		self.configure(cell: cell, with: item)
		
		return cell
	}
	
	func configure(cell: PointCell, with item: PointVM) {
		cell.pointXLabel.text = item.xTitle
		cell.pointYLabel.text = item.yTitle
	}
}
