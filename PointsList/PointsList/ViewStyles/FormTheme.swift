import UIKit


struct FormTheme {

	static let fieldTitleContainer = ViewStyle<UIView> { view in
		view.backgroundColor = Color.clear
	}
	
	static let fieldTitle = ViewStyle<UILabel> { view in
		view.backgroundColor = Color.clear
		view.textColor = ColorName.fieldTitleText.color
		view.font = UIFont.preferredFont(forTextStyle: .title2)
	}

	static let staticTextContainer = ViewStyle<UIView> { view in
		view.backgroundColor = ColorName.staticTextBG.color
	}

	static let staticText = ViewStyle<UILabel> { view in
		view.backgroundColor = Color.clear
		view.textColor = ColorName.staticText.color
		view.font = UIFont.preferredFont(forTextStyle: .body)
	}

	static let inputContainer = ViewStyle<UIView> { view in
		view.backgroundColor = ColorName.inputFieldBG.color
	}

	static let inputField = ViewStyle<UITextField> { view in
		view.backgroundColor = Color.clear
		view.textColor = ColorName.inputFieldText.color
		view.font = UIFont.preferredFont(forTextStyle: .body)
		view.borderStyle = .none
	}
	
	static let ctaButton = ViewStyle<UIButton> { view in
		view.backgroundColor = ColorName.ctaButtonBG.color
		view.setTitleColor(ColorName.ctaButtonTextNormal.color, for: .normal)
		view.setTitleColor(ColorName.ctaButtonTextHighlighted.color, for: .highlighted)
		view.setTitleColor(ColorName.ctaButtonTextDisabled.color, for: .disabled)
		view.titleLabel?.font = UIFont.preferredFont(forTextStyle: UIFont.TextStyle.body)
		view.fixSize(height: 52)
	}
}
