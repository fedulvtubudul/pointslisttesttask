import Foundation


enum ValidationResult {
	case success
	case error(description: String)
}


protocol Validator {
	associatedtype ValueType
	func isValid(value: ValueType?) -> ValidationResult
}


class StringValidator: Validator {
	
	func isValid(value: String?) -> ValidationResult {
		return .success
	}
	
}

class NotBlankValidator: StringValidator {
	
	override func isValid(value: String?) -> ValidationResult {
		
		guard let existent = value else {
			return ValidationResult.error(description: "Field is required")
		}
		
		if existent.isBlank {
			return ValidationResult.error(description: "Field is required")
		}
		
		return ValidationResult.success
	}
	
}

class CharactersSetValidator: StringValidator {
	private let illegalCharacters: CharacterSet
	
	init(illegalCharacters: CharacterSet) {
		self.illegalCharacters = illegalCharacters
		super.init()
	}
	
	override func isValid(value: String?) -> ValidationResult {
		guard let value = value,
			value.rangeOfCharacter(from: self.illegalCharacters) == nil else {
				return ValidationResult.error(description: "Ilegal characters")
		}
		return ValidationResult.success
	}
}


class MinMaxDecimalNumberValidator: StringValidator {
	private var minValue: Int
	private var maxValue: Int
	
	init(minValue: Int = Int.min, maxValue: Int = Int.max) {
		self.minValue = minValue
		self.maxValue = maxValue
	}
	
	override func isValid(value: String?) -> ValidationResult {
		let notBlankValidator = NotBlankValidator()
		let notBlackResult = notBlankValidator.isValid(value:value)
		if case ValidationResult.error(description: _) = notBlackResult {
			return notBlackResult
		}
		
		let legalCharacters = CharacterSet.decimalDigits
		let charactersValidator = CharactersSetValidator(illegalCharacters: legalCharacters.inverted)
		let charactersResult = charactersValidator.isValid(value:value)
		if case ValidationResult.error(description: _) = charactersResult {
			return charactersResult
		}
		
		guard let value = value,
			let enteredNumber = NumberFormatter().number(from: value),
			enteredNumber.intValue >= self.minValue,
			enteredNumber.intValue <= self.maxValue else {
				return ValidationResult.error(description: "Value out of range")
		}
		
		return  ValidationResult.success
	}
}

