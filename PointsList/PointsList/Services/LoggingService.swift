import Foundation
import CocoaLumberjack


protocol LoggingService {
	
}


class LoggingServiceImpl: LoggingService {
	
	init() {
//		DDLog.add(DDTTYLogger.sharedInstance) // TTY = Xcode console
		DDLog.add(DDASLLogger.sharedInstance) // ASL = Apple System Logs

		#if DEBUG
			defaultDebugLevel = DDLogLevel.all
		#else
			defaultDebugLevel = DDLogLevel.warning
		#endif

	}
	
}
