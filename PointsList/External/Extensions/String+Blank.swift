import Foundation


extension String {
	
	var isBlank: Bool {
		get {
			let trimmed = trimmingCharacters(in: CharacterSet.whitespaces)
			return trimmed.isEmpty
		}
	}
	
}
