import UIKit


struct ViewStyle<T> {
	
	let styling: (T)-> Void
	
	static func compose(_ styles: ViewStyle<T>...)-> ViewStyle<T> {
		return ViewStyle { view in
			for style in styles {
				style.styling(view)
			}
		}
	}
	
	func apply(to view: T) {
		styling(view)
	}
}
