import Alamofire


protocol RequestRouter: URLRequestConvertible {
	var method: HTTPMethod { get }
	var path: String { get }
	func baseUrl() -> URL
}


extension RequestRouter {
	
	var fullUrl: URL {
		return self.baseUrl().appendingPathComponent(self.path)
	}
	
	func createUrlWithParametrs(_ param: () -> Parameters? ) throws -> URLRequest {
		var urlRequest = URLRequest(url: self.baseUrl().appendingPathComponent(path))
		urlRequest.httpMethod = method.rawValue
		let encoding: ParameterEncoding = method == .get ? URLEncoding() : JSONEncoding()
		urlRequest = try encoding.encode(urlRequest, with: param())
		return urlRequest
	}

	func createUrlWithParametrs(encoding: ParameterEncoding, _ param: () -> Parameters?) throws -> URLRequest {
		var urlRequest = URLRequest(url: self.baseUrl().appendingPathComponent(path))
		urlRequest.httpMethod = method.rawValue
		urlRequest = try encoding.encode(urlRequest, with: param())
		return urlRequest
	}

}
