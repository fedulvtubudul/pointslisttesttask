import Foundation
import RxSwift
import RxCocoa


protocol ConfigurationVM: class {
	var countInputVM: TextInputVM { get }
	var proceedEnabled: BehaviorRelay<Bool> { get }
	var count: BehaviorRelay<Int?> { get }
}


class ConfigurationVMImpl: ConfigurationVM {
	let countInputVM: TextInputVM
	let proceedEnabled = BehaviorRelay(value: true)
	let count = BehaviorRelay<Int?>(value: nil)
	
	private let disposeBag = DisposeBag()

	init() {
		self.countInputVM = TextInputVM(
			placeholder: L10n.PointsList.Configuration.Count.placeholder,
			initialValue: String()
		)
		
		self.countInputVM.validator = MinMaxDecimalNumberValidator(minValue: 1, maxValue: 200)
		
		let text = self.countInputVM.inputText
		
		text.map({_ in
				return self.countInputVM.validate()
			}).bind(to: self.proceedEnabled)
			.disposed(by: self.disposeBag)
		
		text.map { (string) -> Int? in
				guard let text = string else {
					return nil
				}
			
				return Int(text)
			}.bind(to: self.count)
			.disposed(by: self.disposeBag)
	}
}
