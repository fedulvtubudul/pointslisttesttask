import UIKit
import RxSwift
import RxCocoa


class ConfigurationVC: UIViewController {
	var viewModel: ConfigurationVM!
	let pointsRequested = PublishRelay<ConfigurationVM>()
	
	@IBOutlet private var scrollView: UIScrollView!
	@IBOutlet private var stackView: UIStackView!

	@IBOutlet private var infoContainer: UIView!
	@IBOutlet private var infoTitleContainer: UIView!
	@IBOutlet private var infoTitleLabel: UILabel!
	@IBOutlet private var infoTextContainer: UIView!
	@IBOutlet private var infoTextLabel: UILabel!

	@IBOutlet private var countContainer: UIView!
	@IBOutlet private var countTitleContainer: UIView!
	@IBOutlet private var countTitleLabel: UILabel!
	@IBOutlet private var countInputContainer: UIView!
	@IBOutlet private var countInput: UITextField!

	@IBOutlet private var proceedButton: UIButton!
	
	let disposeBag = DisposeBag()

	// MARK: - Lifecycle

	override func viewDidLoad() {
		super.viewDidLoad()
		self.setupStyle()
		self.setupStaticContent()
		self.bindVM()
	}

	// MARK: - UI actions

	@IBAction
	func proceedButtonTap(_ sender: UIButton) {
		self.view.endEditing(true)
		self.pointsRequested.accept(self.viewModel)
	}

}


// MARK: - Private methods
private extension ConfigurationVC {

	func bindVM() {
		self.countInput.placeholder = self.viewModel.countInputVM.placeholder
	
		self.countInput.rx.text
			.bind(to: self.viewModel.countInputVM.inputText)
			.disposed(by: self.disposeBag)
		
		self.viewModel.countInputVM.isDisabled
			.map({ !$0 })
			.bind(to: self.countInput.rx.isEnabled)
			.disposed(by: self.disposeBag)
		
		self.viewModel.proceedEnabled
			.asDriver(onErrorJustReturn: false)
			.drive(self.proceedButton.rx.isEnabled)
			.disposed(by: self.disposeBag)
	}
	
	func setupStyle() {
		CommonTheme.screenBackground.apply(to: self.view)
	
		let transparentViews: [UIView] = [
			self.scrollView,
			self.stackView,
			self.infoContainer,
			self.infoTitleContainer,
			self.infoTitleLabel,
			self.infoTextLabel,
			self.countContainer,
			self.countTitleContainer,
			self.countTitleLabel,
			self.countInputContainer,
			self.countInput,
		]
		
		transparentViews.forEach(CommonTheme.transparent.apply)
		
		FormTheme.fieldTitleContainer.apply(to: self.infoTitleContainer)
		FormTheme.fieldTitle.apply(to: self.infoTitleLabel)
		
		FormTheme.staticTextContainer.apply(to: self.infoContainer)
		FormTheme.staticText.apply(to: self.infoTextLabel)
		
		FormTheme.fieldTitleContainer.apply(to: self.countTitleContainer)
		FormTheme.fieldTitle.apply(to: self.countTitleLabel)

		FormTheme.inputContainer.apply(to: self.countInputContainer)
		FormTheme.inputField.apply(to: self.countInput)
		
		FormTheme.ctaButton.apply(to: self.proceedButton)
	}
	
	func setupStaticContent() {
		self.infoTitleLabel.text = L10n.PointsList.Configuration.Info.title
		self.infoTextLabel.text = L10n.PointsList.Configuration.Info.text
		
		self.countTitleLabel.text = L10n.PointsList.Configuration.Count.title
		
		self.proceedButton.setTitle(L10n.PointsList.Configuration.Proceed.title, for: .normal)
	}
	
}
