import Foundation
import Alamofire


enum PointsRouter: LEORouter {
	case getPoints(count: Int)
	
	var method: HTTPMethod {
		switch self {
			case .getPoints: return .post
		}
	}
	
	var path: String {
		switch self {
			case .getPoints: return "pointsList"
		}
	}
	
	func asURLRequest() throws -> URLRequest {
		return try createUrlWithParametrs(encoding: URLEncoding()) {
			switch self {
				case .getPoints(let count):
					return [
						"count": count,
						"version": "1.1"
					]
			}
		}
	}
}
