import UIKit
import RxSwift
import RxCocoa
import Arrows
import Panels
import Charts

class ChartPanelVC: UIViewController, Panelable {
	var viewModel: PointsListVM!

	@IBOutlet var headerHeight: NSLayoutConstraint!
	@IBOutlet var headerPanel: UIView!
	@IBOutlet private var titleLabel: UILabel!
	@IBOutlet private var arrow: ArrowView!
	@IBOutlet private var chartView: LineChartView!

	private let disposeBag = DisposeBag()

	override func viewDidLoad() {
		super.viewDidLoad()
		self.curveTopCorners()
		self.setupStyle()
		self.setupStaticContent()
		self.setupChart()
		self.bindVM()
	}
}

private extension ChartPanelVC {
	func setupStyle() {
		PanelTheme.panel.apply(to: self.view)
		CommonTheme.transparent.apply(to: self.headerPanel)
		PanelTheme.title.apply(to: self.titleLabel)
	}
	
	func setupStaticContent() {
		self.titleLabel.text = L10n.PointsList.ChartPanel.title
	}
	
	func setupChart() {
		self.chartView.setScaleEnabled(true)
		self.chartView.pinchZoomEnabled = false
		self.chartView.legend.enabled = false
	}
	
	func bindVM() {
		self.viewModel.points
			.subscribe(onNext: { [weak self] (points) in
				self?.presentPoints(points)
			}).disposed(by: self.disposeBag)
	}
	
	func presentPoints(_ points: [Point]) {
		let entries = points
			.sorted(by: { (p1, p2) -> Bool in
				return p1.x < p2.x
			}).map { (point) -> ChartDataEntry in
				return ChartDataEntry(x: Double(point.x), y: Double(point.y))
			}
		
		let dataSet = LineChartDataSet(values: entries, label: nil)
		dataSet.drawIconsEnabled = false
		dataSet.drawValuesEnabled = false
		dataSet.drawFilledEnabled = false
		
		dataSet.lineDashLengths = [5, 2.5]
		dataSet.highlightLineDashLengths = [5, 2.5]
		dataSet.setColor(ColorName.defaultTint.color)
		dataSet.setCircleColor(ColorName.defaultTint.color)
		dataSet.lineWidth = 1
		dataSet.circleRadius = 3
		dataSet.drawCircleHoleEnabled = true
		
		let data = LineChartData(dataSet: dataSet)
		
		self.chartView.data = data
	}
}

extension ChartPanelVC: PanelNotifications {
	func panelDidPresented() {
		arrow.update(to: .middle, animated: true)
	}
	
	func panelDidCollapse() {
		arrow.update(to: .up, animated: true)
	}
	
	func panelDidOpen() {
		arrow.update(to: .down, animated: true)
	}
	
}
