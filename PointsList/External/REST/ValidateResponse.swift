import Alamofire



extension DataRequest {
    
    @discardableResult
    public func leo_validateResponse() -> Self {
        return self.validate { (request, response, data) -> Request.ValidationResult in
            
            guard let jsonData = data, let jsonString = String(data: jsonData, encoding: .utf8) else {
                return .failure(NetworkLayerError.badResponse(message: "Response is nil or cant convert to utf8 string"))
            }
            
            do {
                let baseResponse = try LEOBaseResponse(JSONString: jsonString)
                let result = baseResponse.result
                guard result == 0 else {
                    return .failure(NetworkLayerError.businessProblem(code: result, errorMessage: baseResponse.message))
                }
                return .success
            } catch {
                return .failure(NetworkLayerError.badResponse(message: error.localizedDescription))
            }
        }
    }
}

