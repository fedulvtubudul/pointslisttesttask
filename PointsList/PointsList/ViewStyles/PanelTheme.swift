import UIKit


struct PanelTheme {

	static let panel = ViewStyle<UIView> { view in
		view.backgroundColor = ColorName.panelBG.color
	}
	
	static let title = ViewStyle<UILabel> { view in
		view.backgroundColor = Color.clear
		view.textColor = ColorName.panelTitleText.color
		view.font = UIFont.preferredFont(forTextStyle: .largeTitle)
	}

}
