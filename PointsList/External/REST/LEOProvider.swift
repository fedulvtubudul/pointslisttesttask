import Alamofire
import AlamofireObjectMapper
import ObjectMapper
import RxSwift
import CocoaLumberjack


class LEOProvider {
	
	let sessionManager: SessionManager
	
	init() {
		let configuration = URLSessionConfiguration.default
		
		let serverTrustPolicies: [String: ServerTrustPolicy] = [
			"demo.bankplus.ru": ServerTrustPolicy.pinCertificates(
				certificates: ServerTrustPolicy.certificates(),
				validateCertificateChain: false,
				validateHost: false)
		]
		
		self.sessionManager = SessionManager(configuration: configuration,
			serverTrustPolicyManager: ServerTrustPolicyManager(policies: serverTrustPolicies))
	}
	
	//MARK: - Request
	@discardableResult
	public func request<T: LEOBaseResponse>(router: LEORouter,
		completionHandler: @escaping (Response<T>) -> Void)
		-> DataRequest {
			
			return self.sessionManager
				.request(router)
				.leo_logRequestCURL { DDLogVerbose($0) }
				.leo_logResponse{ DDLogVerbose($0) }
				.leo_validateResponse()
				.leo_mapResponse(completionHandler: completionHandler)
	}
	
}
