import Foundation
import RxSwift


class PointsServiceMock: PointsService {

	private let step: Float = 0.1

	func fetchPoints(count: Int) -> Single<[Point]> {
		var points = [Point]()
		
		
		let max = self.step * Float(count)
		for x: Float in stride(from: 0.0, to: max, by: self.step) {
			let y = sin(x)
			points.append(Point(x: x, y: y))
		}
		
		return Single<[Point]>.just(points)
	}

}
