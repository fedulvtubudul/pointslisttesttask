import UIKit


struct ListTheme {

	static let listCell = ViewStyle<UITableViewCell> { view in
		view.backgroundView?.backgroundColor = ColorName.listCellBG.color
		view.selectionStyle = .none
	}
	
	static let listCellText = ViewStyle<UILabel> { view in
		view.backgroundColor = Color.clear
		view.textColor = ColorName.listCellText.color
		view.font = UIFont.preferredFont(forTextStyle: .body)
	}

}
