import Foundation
import RxSwift
import RxCocoa


enum PointsListError: Error, CustomStringConvertible {
	case networkError(_ error: NetworkLayerError)
	case unknown
	
	public var description: String {
		switch self {
			case let .networkError(error): return error.description
			case .unknown: return L10n.Error.Unknown.message
		}
	}
	
}



protocol PointsListVM: class {
	var points: BehaviorRelay<[Point]> { get }
	var listItems: BehaviorRelay<[PointVM]> { get }

	var errorOccured: PublishRelay<PointsListError> { get }

	func fetch()
}


class PointsListVMImpl: PointsListVM {
	typealias Context = PointsServiceContext

	let points = BehaviorRelay<[Point]>(value: [])
	let listItems = BehaviorRelay<[PointVM]>(value: [])

	let errorOccured = PublishRelay<PointsListError>()

	private let count: Int
	private let context: Context
	private let disposeBag = DisposeBag()
	
	// MARK: - Lifecycle
	
	init(context: Context, count: Int) {
		self.context = context
		self.count = count
		
		self.points.map({ (points) -> [PointVM] in
				return points.map({ (point) -> PointVM in
					let vm: PointVM = PointVMImpl(point: point)
					return vm
				})
			}).bind(to: self.listItems)
			.disposed(by: self.disposeBag)
	}

	// MARK: - Public methods
	
	func fetch() {
		self.context.pointsService
			.fetchPoints(count: self.count)
			.subscribe(onSuccess: { [weak self] (points) in
					self?.points.accept(points)
				}, onError: { [weak self] (error) in
					self?.points.accept([])
					
					let vmError: PointsListError
				
					if let networkError = error as? NetworkLayerError {
						vmError = .networkError(networkError)
					} else {
						vmError = .unknown
					}
					
					self?.errorOccured.accept(vmError)
				})
			.disposed(by: self.disposeBag)
		}
}
