import ObjectMapper


public enum LEOAPIErrorCode: Error {
    case missingResultCode
    case unknown
}



open class LEOBaseResponse: ImmutableMappable {
	
	public let result: Int
	public let message: String?
	
	required public init(map: Map) throws {
		let successCode: Int? = try? map.value("result")
		let errorCode: Int? = try? map.value("response.result")
		guard let resultCode = successCode ?? errorCode else {
			throw LEOAPIErrorCode.missingResultCode
		}
		
		self.result = resultCode
		
		let globalMessage: String? = try? map.value("message")
		let responseMessage: String? = try? map.value("response.message")
		self.message = globalMessage ?? responseMessage
	}
	
}

extension LEOBaseResponse {
    
    public var isSuccess: Bool {
        if self.result == 0 {
            return true
        }
        return false
    }
}
