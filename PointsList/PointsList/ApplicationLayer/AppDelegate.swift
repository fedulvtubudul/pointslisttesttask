import UIKit


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
	
	var window: UIWindow?
	let context = AppContextImpl()
	var appCoordinator: AppCoordinator!
	
	
	func application(_ application: UIApplication,
		didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
		
		let screenBounds = UIScreen.main.bounds
		self.window = UIWindow(frame: screenBounds)
		self.window?.makeKeyAndVisible()
		
		self.appCoordinator = AppCoordinator(window: self.window!, context: context)
		self.appCoordinator.start()
		
		return true
	}
	
}
