import Foundation
import UIKit
import RxSwift


class MainCoordinator: BaseCoordinator {
	
	typealias Router = UITabBarController
	
	private let router: Router
	private let context: AppContext
	
	private var pointsNC: UIViewController!
	private var aboutNC: UIViewController!
	
	private let disposeBag = DisposeBag()
	
	// MARK: - Lifecycle
	
	init(router: Router, context: AppContext) {
		self.router = router
		self.context = context
	}
	
	override func start() {
		self.pointsNC = self.makePointsModule()
		self.aboutNC = self.makeAboutModule()
		
		router.viewControllers = [
			self.pointsNC,
			self.aboutNC
		]
	}
	
	// MARK: - Private methods
	
	private func makePointsModule() -> UIViewController {
		let nc = UINavigationController()
		
		let coordinator = PointsListCoordinator(router: nc, context: context)
		self.addDependency(coordinator)
		coordinator.start()
		nc.tabBarItem.title = L10n.PointsList.TabItem.title
		nc.tabBarItem.image = Asset.points.image
		
		return nc
	}
	
	private func makeAboutModule() -> UIViewController {
		let nc = UINavigationController()
		
        let coordinator = AboutCoordinator(router: nc, context: context)
        self.addDependency(coordinator)
        coordinator.start()
		
		nc.tabBarItem.title = L10n.About.TabItem.title
		nc.tabBarItem.image = Asset.info.image
		
		return nc
	}
	
}
